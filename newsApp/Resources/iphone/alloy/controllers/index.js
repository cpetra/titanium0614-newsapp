function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.menuView = Alloy.createController("menu", {
        id: "menuView",
        __parentSymbol: $.__views.index
    });
    $.__views.menuView.setParent($.__views.index);
    $.__views.newsView = Alloy.createController("news", {
        id: "newsView",
        __parentSymbol: $.__views.index
    });
    $.__views.newsView.setParent($.__views.index);
    exports.destroy = function() {};
    _.extend($, $.__views);
    Ti.Platform.displayCaps.platformWidth;
    Titanium.App.fireEvent("applicationStarted");
    $.index.open();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;