function __processArg(obj, key) {
    var arg = null;
    if (obj) {
        arg = obj[key] || null;
        delete obj[key];
    }
    return arg;
}

function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menu";
    if (arguments[0]) {
        {
            __processArg(arguments[0], "__parentSymbol");
        }
        {
            __processArg(arguments[0], "$model");
        }
        {
            __processArg(arguments[0], "__itemTemplate");
        }
    }
    var $ = this;
    var exports = {};
    $.__views.menu = Ti.UI.createView({
        backgroundColor: "#000000",
        id: "menu"
    });
    $.__views.menu && $.addTopLevelView($.__views.menu);
    $.__views.labelMenu = Ti.UI.createLabel({
        color: "#ffffff",
        top: "25dp",
        left: "100dp",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        text: "Menu",
        id: "labelMenu"
    });
    $.__views.menu.add($.__views.labelMenu);
    $.__views.tableMenu = Ti.UI.createTableView({
        top: "80dp",
        height: Ti.UI.SIZE,
        id: "tableMenu",
        scrollable: "false"
    });
    $.__views.menu.add($.__views.tableMenu);
    $.__views.backBottom = Ti.UI.createView({
        backgroundColor: "#333333",
        bottom: "0dp",
        height: "150dp",
        id: "backBottom"
    });
    $.__views.menu.add($.__views.backBottom);
    $.__views.tableMenuSecond = Ti.UI.createTableView({
        top: "340dp",
        height: Ti.UI.SIZE,
        id: "tableMenuSecond",
        scrollable: "false"
    });
    $.__views.menu.add($.__views.tableMenuSecond);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var dataTable = [ {
        title: "BBC",
        hasChild: true,
        image: "/images/weblink.png",
        url: "http://m.bbc.com/"
    }, {
        title: "CNN",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://edition.cnn.com/"
    }, {
        title: "FOX",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.fox.com"
    }, {
        title: "CBC",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.cbc.ca/m/touch/"
    }, {
        title: "ABC",
        hasChild: true,
        image: "/images/weblink.png",
        url: "http://abc.go.com/"
    } ];
    var dataTableSectionSecond = [ {
        title: "RSS",
        hasChild: true,
        image: "/images/instagram.png",
        url: "http://feeds.bbci.co.uk/news/rss.xml"
    }, {
        title: "Twitter",
        hasChild: true,
        image: "/images/twitter.png",
        url: "https://mobile.twitter.com/@bbcbreaking"
    }, {
        title: "Youtube",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://m.youtube.com/user/bbcnews"
    }, {
        title: "Facebook",
        hasChild: true,
        image: "/images/instagram.png",
        url: "https://m.facebook.com/"
    } ];
    var data = [];
    for (var i = 0; i < dataTable.length; i++) data.push(Alloy.createController("menuRow", dataTable[i]).getView());
    var dataSectionSecond = [];
    for (var i = 0; i < dataTableSectionSecond.length; i++) dataSectionSecond.push(Alloy.createController("menuRow", dataTableSectionSecond[i]).getView());
    $.tableMenu.setData(data);
    $.tableMenuSecond.setData(dataSectionSecond);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;