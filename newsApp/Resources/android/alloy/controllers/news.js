function Controller() {
    function backButtonHandler() {
        if (openRssDetailWindow) {
            $.webPage.visible = false;
            $.tableRSS.visible = true;
        } else $.webPage.goBack();
    }
    function tapWebHandler() {
        $.navButtons.visible = true;
    }
    function btnMenuHandlerClick() {
        isOpened ? fctAnimateNewsLeft() : fctAnimateNewsRight();
    }
    function itemRowSelected(e) {
        Titanium.API.info(e.title);
        "RSS" == e.title ? changeRssInfos(e) : changePageInfos(e);
        fctAnimateNewsLeft();
    }
    function rssItemClickedHandler() {}
    function changeRssInfos(param) {
        $.tableRSS.visible = true;
        $.title.setText(param.title);
    }
    function changePageInfos(param) {
        $.tableRSS.visible = false;
        $.title.setText(param.title);
        $.container.setTitle(param.title);
        $.webPage.setUrl(param.url);
        $.webPage.visible = true;
        openRssDetailWindow = false;
    }
    function fctAnimateNewsRight() {
        var a = Titanium.UI.createAnimation({
            left: 250,
            duration: 500
        });
        $.news.animate(a);
        $.btnBack.setTitle("");
        isOpened = true;
    }
    function fctAnimateNewsLeft() {
        var a = Titanium.UI.createAnimation({
            left: 0,
            duration: 500
        });
        $.news.animate(a);
        isOpened = false;
        $.btnBack.setTitle("< Back");
    }
    function handlerStates(e) {
        if ("resumed" == e.type) {
            Titanium.API.info(e.type);
            var args = {
                title: "BBC",
                url: "http://www.bbc.com"
            };
            Titanium.App.fireEvent("itemRowSelected", args);
        }
    }
    function appStarted() {
        Titanium.API.info("news open");
        $.news.open();
    }
    function openDetail(e) {
        $.webPage.setUrl(e.row.articleUrl);
        $.webPage.visible = true;
        $.tableRSS.visible = false;
        openRssDetailWindow = true;
    }
    function refreshRss() {
        rss.loadRssFeed(RSS_URL, {
            success: function(data) {
                var rows = [];
                _.each(data, function(item) {
                    rows.push(Alloy.createController("rowRss", {
                        articleUrl: item.link,
                        title: item.title,
                        date: item.pubDate,
                        description: item.description,
                        thumbnail: item.thumbnail
                    }).getView());
                });
                $.tableRSS.setData(rows);
            }
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "news";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.container = Ti.UI.createWindow({
        backgroundColor: "#efefef",
        width: Ti.Platform.displayCaps.platformWidth,
        id: "container",
        modal: "true",
        title: "BBC"
    });
    $.__views.btnBack = Ti.UI.createButton({
        color: "#000000",
        font: {
            fontSize: "18dp"
        },
        backgroundImage: "none",
        id: "btnBack",
        title: "< Back"
    });
    $.__views.container.leftNavButton = $.__views.btnBack;
    $.__views.btnMenu = Ti.UI.createImageView({
        left: "-240dp",
        top: "25dp",
        height: "22dp",
        id: "btnMenu",
        image: "/images/btnMenu.png"
    });
    $.__views.container.rightNavButton = $.__views.btnMenu;
    $.__views.title = Ti.UI.createLabel({
        color: "#000000",
        top: "25dp",
        left: "140dp",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        id: "title",
        text: "BBC",
        visible: "false"
    });
    $.__views.container.add($.__views.title);
    $.__views.webPage = Ti.UI.createWebView({
        top: "0dp",
        bottom: "40dp",
        id: "webPage",
        url: "http://www.bbc.com"
    });
    $.__views.container.add($.__views.webPage);
    $.__views.tableRSS = Ti.UI.createTableView({
        top: "0dp",
        bottom: "40dp",
        left: "0dp",
        id: "tableRSS",
        visible: "false"
    });
    $.__views.container.add($.__views.tableRSS);
    openDetail ? $.__views.tableRSS.addEventListener("click", openDetail) : __defers["$.__views.tableRSS!click!openDetail"] = true;
    $.__views.ad = Ti.UI.iOS.createAdView({
        bottom: 0,
        left: 0,
        height: "50dp",
        width: Ti.Platform.displayCaps.platformWidth,
        id: "ad",
        publisherId: "pub-9520309324412220"
    });
    $.__views.container.add($.__views.ad);
    $.__views.news = Ti.UI.iOS.createNavigationWindow({
        window: $.__views.container,
        id: "news"
    });
    $.__views.news && $.addTopLevelView($.__views.news);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var isOpened = false;
    var rss = require("rss");
    var RSS_URL = "http://feeds.bbci.co.uk/news/rss.xml";
    var openRssDetailWindow = false;
    $.btnMenu.addEventListener("click", btnMenuHandlerClick);
    $.webPage.addEventListener("tap", tapWebHandler);
    Titanium.App.addEventListener("itemRowSelected", itemRowSelected);
    Titanium.App.addEventListener("resume", handlerStates);
    Titanium.App.addEventListener("resumed", handlerStates);
    Titanium.App.addEventListener("paused", handlerStates);
    Titanium.App.addEventListener("applicationStarted", appStarted);
    Titanium.App.addEventListener("rssItemClicked", rssItemClickedHandler);
    $.btnBack.addEventListener("click", backButtonHandler);
    refreshRss();
    __defers["$.__views.tableRSS!click!openDetail"] && $.__views.tableRSS.addEventListener("click", openDetail);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;