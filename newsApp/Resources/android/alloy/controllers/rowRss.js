function Controller() {
    function clickHandler() {
        Titanium.App.fireEvent("rssItemClicked");
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "rowRss";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.row = Ti.UI.createTableViewRow({
        backgroundColor: "#fff",
        height: "115dp",
        className: "itemRow",
        id: "row"
    });
    $.__views.row && $.addTopLevelView($.__views.row);
    $.__views.imgView = Ti.UI.createImageView({
        width: "100dp",
        height: "81",
        left: "10dp",
        top: "10dp",
        id: "imgView"
    });
    $.__views.row.add($.__views.imgView);
    $.__views.viewTextInfo = Ti.UI.createView({
        layout: "vertical",
        left: "110dp",
        top: "0dp",
        id: "viewTextInfo"
    });
    $.__views.row.add($.__views.viewTextInfo);
    $.__views.title = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        top: "0dp",
        font: {
            fontSize: "15dp"
        },
        left: "10dp",
        right: "3dp",
        touchEnabled: false,
        id: "title"
    });
    $.__views.viewTextInfo.add($.__views.title);
    $.__views.description = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        top: "0dp",
        font: {
            fontSize: "12dp"
        },
        left: "10dp",
        right: "3dp",
        touchEnabled: false,
        id: "description"
    });
    $.__views.viewTextInfo.add($.__views.description);
    $.__views.date = Ti.UI.createLabel({
        height: Ti.UI.SIZE,
        width: "68dp",
        left: "15dp",
        top: "5dp",
        color: "#444",
        font: {
            fontSize: "12dp"
        },
        textAlign: "center",
        touchEnabled: false,
        id: "date",
        visible: "false"
    });
    $.__views.viewTextInfo.add($.__views.date);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.row.articleUrl = args.articleUrl;
    $.date.text = args.date;
    $.title.text = args.title;
    $.description.text = args.description;
    $.imgView.setImage(args.thumbnail);
    $.row.addEventListener("click", clickHandler);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;