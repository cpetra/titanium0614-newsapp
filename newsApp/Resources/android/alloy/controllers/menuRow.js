function Controller() {
    function clickHandler() {
        Titanium.App.fireEvent("itemRowSelected", args);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menuRow";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.row = Ti.UI.createTableViewRow({
        backgroundColor: "#333333",
        height: "40dp",
        id: "row"
    });
    $.__views.row && $.addTopLevelView($.__views.row);
    $.__views.imageRow = Ti.UI.createImageView({
        left: "20dp",
        width: "30dp",
        id: "imageRow",
        image: "/images/twitter.png"
    });
    $.__views.row.add($.__views.imageRow);
    $.__views.title = Ti.UI.createLabel({
        left: "70dp",
        color: "#ffffff",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        id: "title",
        text: "Cnn"
    });
    $.__views.row.add($.__views.title);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var args = arguments[0] || {};
    $.title.setText(args.title);
    $.imageRow.setImage(args.image);
    $.row.addEventListener("click", clickHandler);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;