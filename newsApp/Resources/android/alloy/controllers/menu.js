function Controller() {
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "menu";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.menu = Ti.UI.createView({
        backgroundColor: "#000000",
        id: "menu"
    });
    $.__views.menu && $.addTopLevelView($.__views.menu);
    $.__views.labelMenu = Ti.UI.createLabel({
        color: "#ffffff",
        top: "25dp",
        left: "100dp",
        font: {
            fontSize: "18dp",
            fontWeight: "bold"
        },
        text: "Menu",
        id: "labelMenu"
    });
    $.__views.menu.add($.__views.labelMenu);
    $.__views.tableMenu = Ti.UI.createTableView({
        top: "80dp",
        height: Ti.UI.SIZE,
        id: "tableMenu",
        scrollable: "false"
    });
    $.__views.menu.add($.__views.tableMenu);
    $.__views.backBottom = Ti.UI.createView({
        backgroundColor: "#333333",
        bottom: "0dp",
        height: "50dp",
        id: "backBottom"
    });
    $.__views.menu.add($.__views.backBottom);
    $.__views.tableMenuSecond = Ti.UI.createTableView({
        top: "330dp",
        height: Ti.UI.SIZE,
        id: "tableMenuSecond",
        scrollable: "false"
    });
    $.__views.menu.add($.__views.tableMenuSecond);
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    var dataTable = [ {
        title: "BBC",
        hasChild: true,
        image: "/images/weblink.png",
        url: "http://www.bbc.com"
    }, {
        title: "CNN",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.cnn.com"
    }, {
        title: "FOX",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.fox.com"
    }, {
        title: "CBC",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.cbc.ca"
    }, {
        title: "ABC",
        hasChild: true,
        image: "/images/weblink.png",
        url: "http://www.abc.go.com"
    } ];
    var dataTableSectionSecond = [ {
        title: "RSS",
        hasChild: true,
        image: "/images/instagram.png",
        url: "http://feeds.bbci.co.uk/news/rss.xml"
    }, {
        title: "Twitter",
        hasChild: true,
        image: "/images/twitter.png",
        url: "https://twitter.com/@bbcbreaking"
    }, {
        title: "Youtube",
        hasChild: true,
        image: "/images/twitter.png",
        url: "http://www.youtube.com/user/bbcnews"
    }, {
        title: "Facebook",
        hasChild: true,
        image: "/images/instagram.png",
        url: "http://www.facebook.com"
    } ];
    var data = [];
    for (var i = 0; dataTable.length > i; i++) data.push(Alloy.createController("menuRow", dataTable[i]).getView());
    var dataSectionSecond = [];
    for (var i = 0; dataTableSectionSecond.length > i; i++) dataSectionSecond.push(Alloy.createController("menuRow", dataTableSectionSecond[i]).getView());
    $.tableMenu.setData(data);
    $.tableMenuSecond.setData(dataSectionSecond);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;