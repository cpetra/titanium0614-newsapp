var args = arguments[0] || {};

var dataTable = [
    {title:'BBC', hasChild:true, image:'/images/weblink.png', url:'http://m.bbc.com/'},
    {title:'CNN', hasChild:true, image:'/images/twitter.png', url:'http://edition.cnn.com/'},
    {title:'FOX', hasChild:true, image:'/images/twitter.png', url:'http://www.fox.com'},
    {title:'CBC', hasChild:true, image:'/images/twitter.png', url:'http://www.cbc.ca/m/touch/'},
    {title:'ABC', hasChild:true, image:'/images/weblink.png', url:'http://abc.go.com/'}
];


var dataTableSectionSecond = [
	{title:'RSS', hasChild:true, image:'/images/instagram.png', url:'http://feeds.bbci.co.uk/news/rss.xml'},
    {title:'Twitter', hasChild:true, image:'/images/twitter.png', url:'https://mobile.twitter.com/@bbcbreaking'},
    {title:'Youtube', hasChild:true, image:'/images/twitter.png', url:'http://m.youtube.com/user/bbcnews'},
    {title:'Facebook', hasChild:true, image:'/images/instagram.png', url:'https://m.facebook.com/'}
];



var data = [];
for (var i = 0; i < dataTable.length; i++) {
    data.push(Alloy.createController('menuRow', dataTable[i]).getView());
}

var dataSectionSecond = [];
for (var i = 0; i < dataTableSectionSecond.length; i++) {
    dataSectionSecond.push(Alloy.createController('menuRow', dataTableSectionSecond[i]).getView());
}


$.tableMenu.setData(data);

$.tableMenuSecond.setData(dataSectionSecond);

