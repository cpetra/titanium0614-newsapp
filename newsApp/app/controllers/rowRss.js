var args = arguments[0] || {};

$.row.articleUrl = args.articleUrl;
$.date.text = args.date;
$.title.text = args.title;
$.description.text = args.description;

$.imgView.setImage(args.thumbnail);

$.row.addEventListener('click', clickHandler);

function clickHandler(e){
	
	Titanium.App.fireEvent('rssItemClicked');
}
