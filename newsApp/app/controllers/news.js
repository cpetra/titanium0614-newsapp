var args = arguments[0] || {};
var isOpened = false;

var rss = require('rss');
var RSS_URL = 'http://feeds.bbci.co.uk/news/rss.xml';
var openRssDetailWindow = false;

var initURL;

var btnBack = Ti.UI.createButton({
	title: '< Back',
	color: '#000000',
	backgroundImage: 'none',
	font: {
		fontSize: '18dp'
	}
	
});

var emptyView = Ti.UI.createView({});



//events
$.btnMenu.addEventListener('click', btnMenuHandlerClick);
$.webPage.addEventListener('load', loadWebHandler);

Titanium.App.addEventListener('itemRowSelected', itemRowSelected);
Titanium.App.addEventListener('resume', handlerStates);
Titanium.App.addEventListener('resumed', handlerStates);
Titanium.App.addEventListener('paused', handlerStates);
Titanium.App.addEventListener('applicationStarted', appStarted);
Titanium.App.addEventListener('rssItemClicked', rssItemClickedHandler);

btnBack.addEventListener('click', backButtonHandler);


function loadWebHandler(e) {
	Titanium.API.info('load web event');
	Titanium.API.info(($.webPage.canGoBack()));
	Titanium.API.info( $.webPage.url );
	Titanium.API.info( initURL );
	if( $.webPage.canGoBack() && initURL!= $.webPage.url ) {
		Titanium.API.info('if');
		$.container.leftNavButton = btnBack;
	}
	else 
		$.container.leftNavButton = emptyView;
		
};

function backButtonHandler(e) {
	Titanium.API.info('back button handler');
	Titanium.API.info($.webPage.canGoBack());
	
	$.webPage.canGoBack() && initURL!= $.webPage.url  ?   $.container.leftNavButton = btnBack  :  $.container.leftNavButton = emptyView;
	//initURL == $.webPage.url ?  $.container.leftNavButton = btnBack  :  $.container.leftNavButton = emptyView;
	
	if(openRssDetailWindow) {
		$.webPage.visible = false;
		$.tableRSS.visible = true;
		$.container.leftNavButton = emptyView;

	} else {
		$.webPage.goBack();	
	}
	
};

function tapWebHandler(e){
	alert('handler');
}


function btnMenuHandlerClick(e) {
	isOpened ? fctAnimateNewsLeft() : fctAnimateNewsRight();
}

function itemRowSelected(e) {
	Titanium.API.info(e.title);
	(e.title == 'RSS') ? changeRssInfos(e) : changePageInfos(e);
	fctAnimateNewsLeft();
}

function rssItemClickedHandler(e) {
	//alert('dd');
	//$.viewBtnBack.visible = true;
}

function changeRssInfos(param) {
	Titanium.API.info('CHANGE RSS INFO');
	$.tableRSS.visible = true;
	$.container.leftNavButton = emptyView;
	$.container.title = "RSS";
}

function changePageInfos(param) {
	$.tableRSS.visible = false;
	$.title.setText(param.title);
	$.container.setTitle(param.title);
	
	initURL = param.url;
	
	$.webPage.setUrl(param.url);
	$.webPage.visible = true;
	openRssDetailWindow = false;
}

function fctAnimateNewsRight() {
	var a = Titanium.UI.createAnimation({left: 250, duration: 500});
	$.news.animate(a);
	isOpened = true;
}

function fctAnimateNewsLeft() {
	var a = Titanium.UI.createAnimation({left: 0, duration: 500});
	$.news.animate(a);
	isOpened = false;
}

function handlerStates(e) {
	if(e.type == 'resumed') {
		Titanium.API.info(e.type);
		var args = {
			title: "BBC",
			url: "http://www.bbc.com"
		};
		
		Titanium.App.fireEvent('itemRowSelected', args);
	}		
}

function appStarted(e) {
	Titanium.API.info('news open');
	$.news.open();
}

// open detail window
function openDetail(e) {
	initURL = e.row.articleUrl;
	$.webPage.setUrl(e.row.articleUrl);
	$.webPage.visible = true;
	$.tableRSS.visible = false;
	openRssDetailWindow = true;
	
}

// Refresh table data from remote RSS feed
function refreshRss() {
	
	rss.loadRssFeed( RSS_URL, {
		success: function(data) {
		var rows = [];
		_.each(data, function(item) {
			//Titanium.API.info(item.link);
			rows.push(Alloy.createController('rowRss', {
				articleUrl: item.link,
				title: item.title,
				date: item.pubDate,
				description: item.description,
				thumbnail: item.thumbnail
			}).getView());
		});
		$.tableRSS.setData(rows);
	}
	});
}

refreshRss();



//swipe functionality
/*
$.invisbleContainer.addEventListener('swipe', function(e){
	Titanium.API.info('You swiped to the '+e.direction);
	if(e.direction == 'right') {
		fctAnimateNewsRight();
	} else if(e.direction == 'left') {
		fctAnimateNewsLeft();
	}	

});
*/

/*
$.container.addEventListener('touchstart', function(e){
    // Get starting horizontal position
    e.source.axis = parseInt(e.x);
    Titanium.API.info(e.x);
});

$.container.addEventListener('touchmove', function(e) {
	  // Subtracting current position to starting horizontal position
    var coordinates = parseInt(e.x) - e.source.axis;
    // Detecting movement after a 20px shift
    if(coordinates > 20 || coordinates < -20){
        e.source.moving = true;
    }
    var navWindow = $.container;
    // Locks the window so it doesn't move further than allowed
    if(e.source.moving == true && coordinates <= 150 && coordinates >= 0){
        // This will smooth the animation and make it less jumpy
        navWindow.animate({
            left:coordinates,
            duration:20
        });
        // Defining coordinates as the final left position
        navWindow.left = coordinates;
    }
	
});

win.addEventListener('touchend', function(e){
    // No longer moving the window
    e.source.moving = false;
    if(navWindow.left >= 75 && navWindow.left < 150){
        // Repositioning the window to the right
        navWindow.animate({
            left:150,
            duration:300
        });
        menuButton.toggle = true;   
    }else{
        // Repositioning the window to the left
        navWindow.animate({
            left:0,
            duration:300
        });
        menuButton.toggle = false;
    }
});
*/
