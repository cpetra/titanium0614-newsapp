var args = arguments[0] || {};

$.title.setText( args.title );
$.imageRow.setImage( args.image );


//events
$.row.addEventListener('click', clickHandler);


//controller
function clickHandler(e) {
	Titanium.App.fireEvent('itemRowSelected', args);
}